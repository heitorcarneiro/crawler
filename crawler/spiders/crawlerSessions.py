import scrapy
from scrapy import Selector
import json
from datetime import date

class Sala(scrapy.Item):
	nomeSala = scrapy.Field()
	idioma = scrapy.Field()
	formato = scrapy.Field()
	horarios = scrapy.Field() #List

class Filme(scrapy.Item):
	nomeFilme = scrapy.Field()
	salas = scrapy.Field() #List

class Data(scrapy.Item):
	dia = scrapy.Field()
	filmes = scrapy.Field() #List

class Cinema(scrapy.Item):
	nomeCinema = scrapy.Field()
	datas = scrapy.Field() #List

class Cidade(scrapy.Item):
	nomeCidade = scrapy.Field()
	cinemas = scrapy.Field() #List

class CinemarkSpider(scrapy.Spider):
	name = "cinemark"
	#download_delay = 1
	allowed_domains = ["cinemark.com.br"]
	start_urls = ['https://www3.cinemark.com.br/includes/gettheaters']

	page_template = 'https://www3.cinemark.com.br/{}/cinemas'

	def __init__(self, *args, **kwargs):
		super(CinemarkSpider, self).__init__(*args, **kwargs)

		#Iniciando Classes
		self.sala = Sala()
		self.filme = Filme()
		self.filme['salas'] = []
		self.data = Data()
		self.data['filmes'] = []
		self.cinema = Cinema()
		self.cinema['datas'] = []
		self.cidade = Cidade()
		self.cidade['cinemas'] = []

	def parse(self, response):
		theaters = json.loads(response.body_as_unicode())

		for theater in theaters:
			citySlug = theater['CitySlug']
			href = self.page_template.format(citySlug)

			yield scrapy.Request(response.urljoin(href), callback=self.parseSession)

	def parseSession(self, response):
		def extractWithCSS(html, query):
			if (html.css(query).extract() is not None):
				return filter(lambda arg : arg.strip(), html.css(query).extract()) or [u'N/A']

		listaCidades = []
		self.cidade = Cidade()
		self.cidade['cinemas'] = []
		self.cidade['nomeCidade'] = extractWithCSS(response, 'div.location span.city::text')[0]
		listaCidades.append(self.cidade)
		cinemas = extractWithCSS(response, 'div.accordion')
		listaCinemas = []
		for cinema in cinemas:
			selectorCinema = Selector(text=cinema, type='html')
			self.cinema = Cinema()
			self.cinema['datas'] = []
			self.cinema['nomeCinema'] = extractWithCSS(selectorCinema, 'h2.title a::text')[0].strip()
			dias = extractWithCSS(selectorCinema, 'div.tabs-content > div')
			hoje = date.today()
			listaCinemas.append(self.cinema)
			listaDias = []
			for dia in dias:
				selectorDia = Selector(text=dia, type='html')
				self.data = Data()
				self.data['filmes'] = []
				self.data['dia'] = str(hoje)
				filmes = extractWithCSS(selectorDia, 'div.theater')
				hoje = date.fromordinal(hoje.toordinal()+1)
				listaDias.append(self.data)
				listaFilmes = []
				for filme in filmes:
					selectorFilme = Selector(text=filme, type='html')
					self.filme = Filme()
					self.filme['salas'] = []
					self.filme['nomeFilme'] = extractWithCSS(selectorFilme, 'div.theater div.theater-header h3.title ::text')
					salas = extractWithCSS(selectorFilme, 'ul.theater-times > li')
					listaFilmes.append(self.filme)
					listaSalas = []
					for sala in salas:
						selectorSala = Selector(text=sala, type='html')
						self.sala = Sala()
						self.sala['nomeSala'] = extractWithCSS(selectorSala, 'span.times-auditorium ::text')
						self.sala['idioma'] = extractWithCSS(selectorSala, 'ul.times-labels li > span ::text')[0]
						self.sala['formato'] = extractWithCSS(selectorSala, 'ul.times-labels li > span ::text')[1:]
						self.sala['horarios'] = extractWithCSS(selectorSala, 'ul.times-options > li span ::text')
						listaSalas.append(self.sala)

					self.filme['salas'].append(listaSalas)
				self.data['filmes'].append(listaFilmes)
			self.cinema['datas'].append(listaDias)
		self.cidade['cinemas'].append(listaCinemas)
		yield self.cidade
