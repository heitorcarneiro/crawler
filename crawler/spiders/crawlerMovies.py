# -*- coding: utf-8 -*-
import scrapy

class Movie(scrapy.Item):
    nome = scrapy.Field()
    genero = scrapy.Field()
    sinopse = scrapy.Field()
    diretor = scrapy.Field()
    elenco = scrapy.Field()
    duracao = scrapy.Field()
    classificacao = scrapy.Field()
    avaliacao = scrapy.Field()
    trailer = scrapy.Field()
    imagem = scrapy.Field()
    dataEstreia = scrapy.Field()
    distribuidor = scrapy.Field()
    nacionalidade = scrapy.Field()

class AdoroCinemaSpider(scrapy.Spider):
    name = 'adoroCinema'
    #download_delay = 5
    allowed_domains = ['www.adorocinema.com']
    start_urls = ['http://www.adorocinema.com/filmes/todos-filmes/notas-espectadores/']

    page_template = 'http://www.adorocinema.com/filmes/todos-filmes/notas-espectadores/?page={}'

    def __init__(self, *args, **kwargs):
        super(AdoroCinemaSpider, self).__init__(*args, **kwargs)
        self.next_page = 1
        #self.max_page = 1344 (26867 movies)

    def parse(self, response):
        max_page = response.css('li span::text')[-1].extract()
        #max_page = 2
        #Follow Links to Movie
        for href in response.css('div.data_box h2 a::attr(href)').extract():
            yield scrapy.Request(response.urljoin(href), callback=self.parseMovie)

        # Follow Pagination Links
        self.next_page += 1
        if self.next_page <= max_page:
            url = self.page_template.format(self.next_page)
            yield scrapy.Request(url, callback=self.parse)

    def parseMovie(self, response):
        def extractWithCSS(query):
            if (response.css(query).extract() is not None):
                return filter(lambda arg : arg.strip(), response.css(query).extract()) or [u'N/A']

        def extractWithXPath(query):
            if (response.xpath(query).extract() is not None):
                return filter(lambda arg : arg.strip(), response.xpath(query).extract()) or [u'N/A']

        def convertToMinutes(hour):
            try:
                hour = hour.replace('h', '').replace('min', '').split()
                hour = int(hour[0]) * 60 + int(hour[1])
                return hour
            except:
                return 'N/A'

        movie = Movie()

        movie['nome'] = extractWithCSS('div.titlebar-page div.titlebar-title::text')
        movie['genero'] = extractWithXPath("//span[@itemprop='genre']/text()")
        movie['sinopse'] = extractWithCSS('div.synopsis-txt ::text')
        movie['diretor'] = extractWithXPath("//span[@itemprop='director']/a/span/text()")
        movie['elenco'] = extractWithXPath("//div[contains(span, 'Elenco:')]/span/text()")[1:-1]
        movie['duracao'] = convertToMinutes(extractWithXPath("//div[contains(span, 'Data de lan')]/text()")[-1].replace('(', '').replace(')', ''))
        #movie['classificacao'] = extractWithCSS('')
        #movie['avaliacao'] = extractWithCSS('')
        #movie['trailer'] = extractWithCSS('')
        #movie['imagem'] = extractWithCSS('')
        movie['dataEstreia'] = extractWithXPath("//div[contains(span, 'Data de lan')]/strong/span/text()")
        movie['distribuidor'] = extractWithXPath("//span[@itemprop='productionCompany']/text()")
        movie['nacionalidade'] = extractWithXPath("//div[contains(span, 'Nacionalidade')]/span/text()")[1:]

        yield movie
